module ViewHelpers
  def stylesheet_components_link_tag(link)
    return '<link href="/assets/javascripts/vendors/' + link + '.css" media="screen" rel="stylesheet" type="text/css">'
  end
  def javascript_components_include_tag(link)
    return '<script src="/assets/components/' + link + '.js" type="text/javascript"></script>'
  end

  # 主選單項目
  def menuData()
  return [
    {
    title: "關於我們",
    href: "/views/about/about.html"
    }, {
    title: "門市查詢",
    href: "/views/about/stores.html"
    }, {
    title: "聯絡我們",
    href: "/views/about/contact.html"
    }, {
    title: "個資聲明",
    href: "/views/about/personal_information.html"
    }, {
    title: "購物指南",
    href: "",
    subItem: [
      {
      title: "購物須知",
      href: "/views/guide/guide1.html"
      }, {
      title: "尺寸指南",
      href: "/views/guide/guide2.html"
      }, {
      title: "退換貨申請",
      href: "/views/guide/guide3.html"
      }, {
      title: "隱私權保護",
      href: "/views/guide/guide4.html"
      }, {
      title: "送貨說明",
      href: "/views/guide/guide5.html"
      }, {
      title: "常見問題",
      href: "/views/guide/guide6.html"
      }
    ]
    }, {
    title: "登入",
    href: "/views/member/login.html"
    }
  ]
  end

  # 主選單內的社群連結項目
  def sociallyItem()
  return [
    {
    type: "fa-facebook-square",
    href: "#"
    }, {
    type: "fa-youtube",
    href: "#"
    }, {
    type: "fa-instagram",
    href: "#"
    }
  ]
  end

  # Tab Bar
  def tabBar()
  return [
    {
    type: "fa-home",
    href: "/",
    title: "首頁",
    }, {
    type: "fa-map-marker",
    href: "/views/about/stores.html",
    title: "門市查詢",
    }, {
    href: "/views/mypage/mypage.html",
    title: "我的帳戶",
    }, {
    type: "fa-shopping-cart",
    href: "/views/product/product-cart.html",
    title: "購物車",
    }, {
    type: "fa-question",
    href: "/views/about/contact.html",
    title: "H:ELP",
    }
  ]
  end

  def privacyRules()
    return [{
    dot: "第一條",
    name: "會員資料提供與保護",
    content: "<p>申請人應依本網站提供之會員申請表格內容，詳實提供正確且完整之個人資料。前述資料如有異動，會員應主動即時通知H:CONNECT客服中心，本公司並得對會員之資格進行審查核駁。</p><p>本網站對於會員提供之資料，得不定時進行形式審查。如有任何經本網站認為不實、錯誤或有異常之資料，本網站得逕行予以刪除或終止其會員資格。倘有涉及不法嫌疑，本網站並得主動向司法機關舉發。</p><p>申請人如未滿二十歲者，應事先經其法定代理人同意及瞭解後，始得申請加入成為H:CONNECT會員。申請程序完畢，即視為申請人已取得法定代理人之同意及瞭解。如有違誤，申請人及其法定代理人應自負完全法律責任。 申請人(即會員)同意H:CONNECT得將此基本資料提供其公司內部，於台灣地區不限時間為蒐集、處理及利用，以進行活動訊息傳遞、行銷商品及提供服務之用。申請人並得請求查詢、閱覽、複製、補充、更正資料;或請求停止蒐集、處理、利用或刪除資料。個人資料應完整填寫，如填寫不實或不完整，致無法取得會員資格，將無法享有活動訊息傳遞、行銷商品及提供服務等之權益。</p>"
    }, {
    dot: "第二條",
    name: "密碼與帳號的保護",
    content: "<p>申請人於閱讀完本聲明書後，按下「同意」之按鍵經完成申請加入或登錄程序後，即取得H:CONNECT網路會員資格並可取得一帳號及密碼，作為日後進入本公司購物網站時之憑證。本網站並依密碼與帳號作為辨識之依據。 為維持會員個人之權益，已取得網路登錄權之H:CONNECT會員均應自行維持其密碼與帳號之機密安全，以同一個會員帳號和密碼使用會員服務所進行的所有行為，都將被認為是這位會員本人的行為，會員將負完全的責任。同時本網站將依相關法令辦理安全維護事項，防止個人資料被竊取、竄改、毀損、滅失或洩漏，如有發生外洩、盜用或有其他影響密碼或帳號安全情事者，會員應立即通知本網站，本公司將於接獲消費者通知後，停止該進行交易之處理及利用或採取其他保護之措施，但上述通知不代表本公司對會員負有任何形式之賠償或補償的責任。</p><p>前項情事除因本網站故意或重大過失所致之損害者外，概由會員自行負責。</p><p>本網站經消費者請求提供被冒用期間之交易資訊相關資料，經確認無誤後，本網站得於資訊可得範圍內提供相關資料，以防止損害擴大。對於消費者個人資料之蒐集、處理及利用，本公司得於與本聲明書所載之特定目的之相關範圍內，以誠實信用方法為之。契約目的消失或期限屆滿時之個人資料處理，本公司亦秉持誠信原則，處理及利用該相關資料。</p>"
    }, {
    dot: "第三條",
    name: "網站連結",
    content: "<p>緣網路科技日新月異，會員使用本網站之各項服務時，亦可能連結或接觸其他網站。惟非經本網站正式書面同意，其他網站之架設均屬侵害本網站之權益。</p><p>前項其他網站之內容與服務，非屬本網站提供之服務範圍，會員應拒絕進入該網站。如有違悖，其因而造成之損害或爭議，本網站不負任何法律責任。</p>"
    }, {
    dot: "第四條",
    name: "網站使用規定",
    content: "<p>會員應依下列規定使用本網站提供之各項服務：</p><ul><li>不得登載或寄發各種廣告或有廣告嫌疑之圖形、文字或其他表徵。</li><li>不得冒用他人之個人資料、密碼或帳號使用本網站。</li><li>不得公佈、傳輸或散佈電腦病毒或其他違反公序良俗、法令規章之圖形、文字或其他表徵。</li><li>非經本網站同意，不得進行或引誘第三人為買賣、租賃、贈送或其他具有商業性質之行為。</li><li>不得進行侵害或疑似侵害本網站或第三人之名譽權、隱私秘密權、智慧財產權或其他權利之行為。</li><li>不得為其他違反法令規章或經本網站認為不適當之行為。</li></ul><p>本網站得就會員使用情形不定時予以審查。如發現有違反前項規定之情事者，本網站得逕行予以刪除或終止其會員資格。倘有涉及不法嫌疑，本網站並得主動向司法機關舉發。</p>"
    }, {
    dot: "第五條",
    name: "服務暫停與中斷",
    content: "<p>本系統或功能可能因『例行性』之維護、改置或變動發生 服務暫停或中斷，本網站將於該暫停或中斷前以公告或其他適當之方式告知會員。本網站將以合理之方式及技術，維護會員服務之正常運作。但如有下列任一情況， 本網站將逕行暫停或中斷會員服務之全部或一部，且對因此所造成任何不便或損害，均不負任何賠償或補償之責任：</p><ul>
           <li>使用者有任何違反政府法令或本使用條款情形。</li><li>天災或其他不可抗力之因素所導致之服務停止或中斷。</li>
           <li>非本網站所得控制之事由而致會員服務資訊顯示不正確、或遭偽造、竄改、刪除或擷取、或致系統中斷或不能正常運作時。</li><li>對會員服務相關軟硬體設備進行搬遷、更換、升級、保養或維修時。</li><li>其他不可歸責於本網站之事由所致之服務停止或中斷。</li></ul>"
    }, {
    dot: "第六條",
    name: "智慧財產權之聲明",
    content: "<p>本網站所使用之軟體或程式、公佈之所有圖文、檔案、資訊、網頁設計或其他內容、表徵，除法律另有規定外，均擁有智慧財產權與其他權利。任何人非權利人事先書面同意，不得逕自重製、改作、散佈或為其他妨礙權利人權益之行為。</p>"
    }, {
    dot: "第七條",
    name: "責任限制",
    content: "<p>會員使用本網站時，在使用過程中所有的資料紀錄，以本 網站資料庫所記錄之資料為準。如有發生任何糾紛，雙方將以最大誠意提出各自所儲存之電子資料記錄提交於法院或公正第三人作認定。</p>"
    }, {
    dot: "第八條",
    name: "隱私權保障",
    content: "<p>關於會員註冊以及其他特定資料，都將受到《H:CONNECT隱私權政策》之保護與規範。但有下列情形時，不在此限：</p><ul><li>基於法律之規定、或受司法機關與其他有權機關基於法定程序之要求。</li><li>在緊急情況下為維護其他會員或第三人之合法權益。</li><li>您的行為違反會員約定條款。</li><li>為維護會員服務系統的正常運作。</li></ul>"
    }, {
    dot: "第九條",
    name: "資料保密",
    content: "<p>本公司因交易而知悉或持有消費者之交易紀錄及其他相關資料，將進行保密，並建置符合交易需求之安全機制。若發生不當使用個人資料情形，致消費者受有損害時，將依相關法律負賠償責任。</p>"
    }, {
    dot: "第十條",
    name: "本聲明書之效力、準據法與管轄法院",
    content: "<p>本聲明書中，任何條款之全部或一部份無效時，不影響其他約定之效力。會員與本公司之權利義務關係，應依網路規範及中華民國法令解釋及規章、慣例為 依據處理。若產生任何爭議以台灣台北地方法院為第一審管轄法院。本公司的任何聲明、條款如有未盡完善之處，將以最大誠意，依誠實信用、平等互惠原則，共商解決之道。</p>"
    }]
  end

  def faq()
    return [{
    dot: "Q",
    name: "該如何申請商品退換貨呢？",
    content: "<h3>【換貨】</h3><ul><li>H:CONNECT購物官網提供只退貨不換貨之服務。</li><li>若您尺寸不合，您可持發票至全省H:CONNECT門市更換其他尺寸。</li><li>若您收到不合適、不喜愛的商品，您亦可持發票至全省 H:CONNECT門市更換其他商品，所更換價格依門市現場的售價為準。</li><li>如果您所購買的是貼身商品(如內衣、內褲)，為保障顧客權益，基於個人衛生，恕無法提供退換貨服務</li></ul><h3>【退貨】</h3><p>※若您尺寸不合，您可持發票至全省H:CONNECT門市更換其他尺寸。</p>
          <p>※若您收到不合適、不喜愛的商品，您亦可持發票至全省H:CONNECT門市更換其他商品，所更換價格依門市現場的售價為準。</p>"
    }, {
    dot: "Q",
    name: "該如何加入會員呢？",
    content: "<p>若您尚未申請H:CONNECT會員，您可至頁面上方點選「註冊」輸入基本資料，即可成為會員並擁有網路帳號，立即開始購物！</p><p>若您已為會員，請點選「註冊」「實體會員申請網路帳戶」進行帳號合併，請依指示完成認證後，即可同步實體會員相關資訊及紅利點數。</p><p class='warning-color'>**提醒您：若您無法順利收到認證信件，請先至「垃圾信件夾」再次確認，並將郵件設定為非垃圾郵件，藉此可避免因收不到H:CONNECT訂單信函、商品到貨訊息及錯過優惠訊息的遺憾。</p><p class='warning-color'>若您確認垃圾信件夾後，依然沒有收到認證信函，請直接與我們客服聯繫0800-698-800。</p>"
    }, {
    dot: "Q",
    name: "忘記密碼怎麼辦？",
    content: "<p>若您忘記密碼，請點選「會員登入」後，請您先點選「忘記密碼」 再輸入註冊的電子郵件，系統即會自動將密碼重設通知寄至您預設的Email信箱中，請您收取郵件並點擊內文中的連結進行密碼重設即可。</p>"
    }, {
    dot: "Q",
    name: "該如何修改個人資料及密碼？",
    content: "<p>請點選「登入」，輸入您的E-mail及密碼登入會員帳戶，點選「My Page」→「修改會員資料」即可。</p><p>您可由此變更您登入密碼或寄送地址資料，如您需更新聯絡電話，請於下單時填入正確聯絡電話即可。</p>"
    }, {
    dot: "Q",
    name: "如何查詢目前訂單的處理情況？",
    content: "<p>請點選「登入」，輸入您的E-mail及密碼登入後，點選「My Page」，即可查詢該訂單的處理狀態。</p>"
    }]
  end

  def productInfo()
    return [{
    dot: "",
    name: "商品規格",
    content: "<dl class='module-dl'><dt>季節</dt><dd>S/S</dd><dt>類別</dt><dd>女裝</dd><dt>次分類</dt><dd>Pants</dd><dt>材質</dt><dd>其他</dd><dt>產地</dt><dd>韓國設計 中國製造</dd></dl>"
    }, {
    dot: "",
    name: "尺寸表",
    content: '  <p>請直接在身上測量</p><ul><li>測量時請穿著合身內衣，並於胸部豐滿處測量</li><li>在腰部最細的部位量度腰圍</li><li>臀圍的寬度應圍繞臀部最豐滿處測量</li><li>褲內側長度應已褲襠至地面的長度為準</li></ul><table class="module-table table-style-b"><thead><tr><td>WOMEN</td><td>F</td><td>S</td><td>M</td></tr></thead><tbody><tr><td>胸圍(公分)</td><td>88</td><td>88</td><td>96</td></tr><tr><td>腰圍(公分)</td><td>69</td><td>69</td><td>76</td></tr>
    <tr><td>臀圍(公分)</td><td>90</td><td>90</td><td>98</td></tr><tr><td>衣長(公分)</td><td>77</td><td>77</td><td>79</td></tr></tbody></table><table class="module-table table-style-b"><thead><tr><td>WOMEN</td><td>S</td><td>M</td></tr></thead><tbody><tr><td>英吋</td><td>25/26/27</td><td>28/29</td></tr></tbody></table>' 
    }, {
    dot: "",
    name: "寄送及退貨說明",
    content: "<h3>【換貨】</h3><ul><li>H:CONNECT購物官網提供只退貨不換貨之服務。</li><li>若您尺寸不合，您可持發票至全省H:CONNECT門市更換其他尺寸。</li><li>若您收到不合適、不喜愛的商品，您亦可持發票至全省 H:CONNECT門市更換其他商品，所更換價格依門市現場的售價為準。</li><li>如果您所購買的是貼身商品(如內衣、內褲)，為保障顧客權益，基於個人衛生，恕無法提供退換貨服務</li></ul><h3>【退貨】</h3><p>※若您尺寸不合，您可持發票至全省H:CONNECT門市更換其他尺寸。</p>
          <p>※若您收到不合適、不喜愛的商品，您亦可持發票至全省H:CONNECT門市更換其他商品，所更換價格依門市現場的售價為準。</p>"
    }]
  end

  def icon(type, cla)
    case type
      when "logob"
        return '<i>
                  <svg role="img" version="1.1" class="logob ' + cla + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="1.46em" height="1.6em" viewBox="0 0 100 108" style="enable-background:new 0 0 100 108;" xml:space="preserve">
                    <g id="logo-group">
                      <polygon id="c" style="fill:#000000;" points="62.8,92.3 62.8,108 99.7,92.3  "/>
                      <rect id="bg" width="100" height="92.3" />
                      <polygon id="H" style="fill:#FFFFFF;" points="49.1,40.1 36.4,40.1 36.4,25.5 22.7,25.5 22.7,66.8 36.4,66.8 36.4,52.2 49.1,52.2 
                        49.1,66.8 62.8,66.8 62.8,25.5 49.1,25.5 " />
                      <path id="dot-b" style="fill:#FFFFFF;" d="M77.2,67L77.2,67c-4.1,0-7.3-3.3-7.3-7.3l0,0c0-4,3.3-7.3,7.3-7.3l0,0
                        c4,0,7.3,3.3,7.3,7.3l0,0C84.6,63.7,81.3,67,77.2,67z" />
                      <path id="dot-t" style="fill:#FFFFFF;" d="M77.3,39.9L77.3,39.9c-4.1,0-7.3-3.3-7.3-7.3l0,0c0-4,3.3-7.3,7.3-7.3l0,0
                        c4,0,7.3,3.3,7.3,7.3l0,0C84.6,36.7,81.3,39.9,77.3,39.9z" />
                    </g>
                  </svg>
                </i>' 
      when 2
        return "it was 2"
    end
  end
end