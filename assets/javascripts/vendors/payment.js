'use strict';
var html_del_ok_btn = '<div class="btn-popup style-xshort"><a href="">刪除</a></div>'; /*購物車頁面的刪除商品鈕 (物品消失特效未作) */
var html_del_cancel_btn = '<div class="btn-popup style-xshort lb-cancel">加到願望清單</div>'; /*購物車頁面的刪除商品-移到wishlist鈕 (物品消失特效未作) */
(function() {
  //load default user info info fancy btn
  $(".btn-fancy-coupon").find(".btn-fancy-userinfo").text( "未使用" );
  $(".btn-fancy-usepoint").find(".btn-fancy-userinfo").text( $("body").find("input[name='point']").val() );
  $(".btn-fancy-pay").find(".btn-fancy-userinfo").text("未選擇");
  $(".btn-fancy-sendfrom").find(".btn-fancy-userinfo").text($("body").find("input[name='EXSAMPLE_sendname']").val());
  $(".btn-fancy-sendto").find(".btn-fancy-userinfo").text($("body").find("input[name='EXSAMPLE_receivername']").val());
  $(".btn-fancy-invoice").find(".btn-fancy-userinfo").text("未選擇");

  //Cart button controller
  $(".module-cart-size-wrap").on('click', '.btn-cart-sq-size', function() {
    $(this).closest(".module-cart-size-wrap").find(".btn-cart-sq-size").removeClass('choosed');
    $(this).addClass("choosed");
  });
  $(".module-cart-color-wrap").on('click', '.btn-cart-sq-color', function() {
    $(this).closest(".module-cart-color-wrap").find(".btn-cart-sq-color").removeClass('choosed');
    $(this).addClass("choosed");
  });
  $(".module-cart-amount-wrap").on('click', '.btn-cart-amount-minus', function() {
    var num = $(this).closest(".module-cart-amount-wrap").find(".module-cart-item-num").text();
    if( num>1){
      num--;
      $(this).closest(".module-cart-amount-wrap").find(".module-cart-item-num").text(num);
    }
  });
  $(".module-cart-amount-wrap").on('click', '.btn-cart-amount-add', function() {
    var num = $(this).closest(".module-cart-amount-wrap").find(".module-cart-item-num").text();
    num++;
      $(this).closest(".module-cart-amount-wrap").find(".module-cart-item-num").text(num);
  });
  $(".module-cart-item").on('click', '.fa-times', function() {
    var popupContent = '<i class="fa fa-times fa-lg btn-popclosex"></i><h2 class="tcenter">確定刪除此商品</h2><div class="tcenter">'+ html_del_ok_btn + html_del_cancel_btn +'</div>';
    HFeature.popup(popupContent);
    HFeature.popupOpen();
  });
  $("body").on('click', '.lb-cancel,.layout-popup-background,.btn-popclosex', function() {
    HFeature.popupClose();
  });

  //Payment
  // $(".layout-cart-payment").on('click', '.btn-cart-payment-more,.module-cart-list,.layout-cart-bottom-summary', function() {
  //   $(".layout-cart-payment").toggleClass('is-more');
  // });


  $(".btn-fancy-coupon").on('click', function(){
    var lbCoupon = $(".section-coupon").html();
    HFeature.popup(lbCoupon);
    HFeature.popupOpen();
  });

  $(".btn-fancy-usepoint").on('click', function(){
    var lbMypoint = $(".section-mypoint").html();
    HFeature.popup(lbMypoint);
    HFeature.popupOpen();
  });

  $(".btn-fancy-pay").on('click', function(){
    var lbMethod = $(".section-method").html();
    HFeature.popup(lbMethod);
    HFeature.popupOpen();
  });

  $(".btn-fancy-sendfrom").on('click', function(){
    var lbSender = $(".section-sender").html();
    HFeature.popup(lbSender);
    HFeature.popupOpen();
  });

  $(".btn-fancy-sendto").on('click', function(){
    var lbReceive = $(".section-receive").html();
    HFeature.popup(lbReceive);
    HFeature.popupOpen();
  });

  $(".btn-fancy-invoice").on('click', function(){
    var lbInvoicer = $(".section-invoice").html();
    HFeature.popup(lbInvoicer);
    HFeature.popupOpen();
  });

  $("body").on('click',".btn-cancel", function(){
    HFeature.popupClose();
  });

  $("body").on('click',".btn-ok-coupon", function(){
    var myvalue = $("body").find(".layout-popup-wrap").find("input[name='coupon']").val();
    $(".btn-fancy-coupon").find(".btn-fancy-userinfo").text(myvalue);
    $("body").find("input[name='coupon']").attr("value",myvalue);
    HFeature.popupClose();
  });

  $("body").on('click',".btn-ok-mypoint", function(){
    var myvalue = $("body").find(".layout-popup-wrap").find("input[name='point']").val();
    $(".btn-fancy-usepoint").find(".btn-fancy-userinfo").text(myvalue);
    $("body").find("input[name='point']").attr("value",myvalue);
    HFeature.popupClose();
  });

  $("body").on('click',".btn-ok-method", function(){
    var myvalue = $("body").find(".layout-popup-wrap").find("input[name='method']:checked").val();
    $(".btn-fancy-pay").find(".btn-fancy-userinfo").text($("body").find(".layout-popup-wrap").find("input[name='method']:checked+i+span").text());
    $(".section-method").find("input[name='method']").attr('checked', false).eq(myvalue).attr('checked', true);
    HFeature.popupClose();
  });

  $("body").on('click',".btn-ok-sender", function(){
    var myvalue = $("body").find(".layout-popup-wrap").find("select[name='city']:selected").val();
    var myvalue2 = $("body").find(".layout-popup-wrap").find("select[name='region']:selected").val();
    var myvalue3 = $("body").find(".layout-popup-wrap").find("input[name='zip']").val();
    var myvalue4 = $("body").find(".layout-popup-wrap").find("input[name='address']").val();
    var myaddress = $("body").find(".layout-popup-wrap").find("select[name='city'] option:selected").text()
     + $("body").find(".layout-popup-wrap").find("select[name='region'] option:selected").text()
     + $("body").find(".layout-popup-wrap").find("input[name='address']").text();
    $(".btn-fancy-sendfrom").find(".btn-fancy-userinfo").text(myaddress);
    $(".section-sender").find("select[name='city'] option").attr('selected', "").eq(myvalue).attr('selected', "selected");
    $(".section-sender").find("select[name='region'] option").attr('selected', "").eq(myvalue2).attr('selected', "selected");
    $(".section-sender").find("input[name='zip']").attr("value",myvalue3);
    $(".section-sender").find("input[name='address']").attr("value",myvalue4);
    HFeature.popupClose();
  });

  $("body").on('click',".btn-ok-receive", function(){
    var myvalue = $("body").find(".layout-popup-wrap").find("select[name='city']:selected").val();
    var myvalue2 = $("body").find(".layout-popup-wrap").find("select[name='region']:selected").val();
    var myvalue3 = $("body").find(".layout-popup-wrap").find("input[name='zip']").val();
    var myvalue4 = $("body").find(".layout-popup-wrap").find("input[name='address']").val();
    var myvalue5 = $("body").find(".layout-popup-wrap").find("input[name='sameassender']").is(':checked');
    var myaddress = 
    $("body").find(".layout-popup-wrap").find("select[name='city'] option:selected").text()
     + $("body").find(".layout-popup-wrap").find("select[name='region'] option:selected").text()
     + $("body").find(".layout-popup-wrap").find("input[name='address']").text();
    $(".btn-fancy-sendto").find(".btn-fancy-userinfo").text(myaddress);
    $(".section-receive").find("input[name='sameassender']").attr('checked', myvalue5);
    $(".section-receive").find("select[name='city'] option").attr('selected', "").eq(myvalue).attr('selected', "selected");
    $(".section-receive").find("select[name='region'] option").attr('selected', "").eq(myvalue2).attr('selected', "selected");
    $(".section-receive").find("input[name='zip']").attr("value",myvalue3);
    $(".section-receive").find("input[name='address']").attr("value",myvalue4);
    HFeature.popupClose();
  });

  $("body").on('click',".btn-ok-invoice", function(){
    var myvalue = $("body").find(".layout-popup-wrap").find("input[name='invoice']:checked").val();
    var myvalue2 = $("body").find(".layout-popup-wrap").find("input[name='invoice-3sheet-sn']").val();
    var myvalue3 = $("body").find(".layout-popup-wrap").find("input[name='invoice-3sheet-title']").val();
    $(".section-invoice").find("input[name='invoice']").attr('checked', false).eq(myvalue).attr('checked', true);
    $("body").find("input[name='invoice-3sheet-sn']").attr("value",myvalue2);
    $("body").find("input[name='invoice-3sheet-title']").attr("value",myvalue3);
    $(".btn-fancy-invoice").find(".btn-fancy-userinfo").text($("body").find(".layout-popup-wrap").find("input[name='invoice']:checked+i+span").text());

    HFeature.popupClose();
  });

})()