'use strict';

(function() {
  var headerHeight = hVariable.getHeaderHeight();
  $(".accordion-item").find(".accordion-item-title").on('click', function() {
    $(this).parent().toggleClass('on').find(".fa").toggleClass('fa-rotate-180');
    
    //only when opening, scroll this item to screen top
    if( $(this).parent().hasClass('on') ){
      //in product detail page
      if( $(".layout-product-detail").length > 0 ){
        // var detailimgoffset = parseInt( $(".layout-product-detail-imagewrap").height() );
        var newoffset = parseInt( $(this).offset().top );
        var nowscroll = parseInt( $(".layout-product-detail").scrollTop() );
        $('.layout-product-detail').animate({ scrollTop:( nowscroll + newoffset - headerHeight )}, 300); 
      } else
      {
        //in guide page
        var newoffset = parseInt( $(this).offset().top );
        var nowscroll = parseInt( $(".layout-view").scrollTop() );
        $('.layout-view').animate({ scrollTop:( nowscroll + newoffset - headerHeight )}, 300); 
      }
    }

  });
})()