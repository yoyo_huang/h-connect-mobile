'use strict';

(function() {

  // header < and > btn go back and forward
  $('.layout-header-goback').on('click', goBack);
  $('.layout-header-goahead').on('click', goForward);

  function goBack() {
      window.history.back();
  }
  function goForward() {
      window.history.forward();
  }

})()