var url_gps_destination = 'store_detail.html';/*測完經緯度後跳轉的店鋪頁面位址 (測店鋪的函式沒有寫) */

function getLocation() {
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
      alert("Geolocation is not supported by this browser.");
  }
}

function showPosition(position) {
  // alert("your Lat: " + position.coords.latitude +  ",your Lng: " + position.coords.longitude); 
  window.location.href = url_gps_destination;
}

$("#btn-search-store").on('click', function(){
  // alert("please wait...");
  $(this).find("a").html('FINDING STORE... <i class="fa fa-spinner fa-spin"></i>');
  getLocation();
});