'use strict';
var index_full_ad_img = "assets/images/ad-index.png"; /*首頁全幅廣告圖片位址*/

(function() {
  //enter first time, show tutorial
  var popupContent = '<img src=' + index_full_ad_img + ' alt="" width="100%">';
  // HFeature.popup(popupContent);
  // HFeature.popupOpen();
  $(".layout-popup-wrap").addClass("style-nocolor");
  $(".layout-popup-background").addClass("style-indexpopup");
  $("body").on('click','.layout-popup-background,.layout-popup-wrap', function() {
    HFeature.popupClose();
  });

})()