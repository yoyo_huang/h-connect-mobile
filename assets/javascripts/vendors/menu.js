'use strict';

(function() {
  //tabbar Circle number
  $(".layout-tabbar-list-link .fa-shopping-cart").attr("data-cartnum","88");

  // Switch Menu
  $('.layout-header-bars').on('click', closeMenu);


  // Close Menu
  function closeMenu(event) {
    $('body').toggleClass('layout-menu-open');
    if($(event.target).closest('.layout-header-bars').length && $('body').hasClass('layout-menu-open')) {
      $('.layout-view').on('click', closeMenu);
    } else {
      $('.layout-view').off('click');
    }
  };


  // Open Sub Menu
  $('.have-sub-item').on('click', '.trigger, .fa-angle-down', function(event) {
    console.log("ff");
    var _l = $('.layout-menu-sublist').height() > 0 ? 0 : $('.layout-menu-list-subitem').length,
        _h = $('.layout-menu-list-subitem').height();
    
    $('.layout-menu-sublist').css({
      maxHeight: _l * _h + 'px',
    });

    $(this)
      .siblings('.fa')
      .toggleClass('fa-flip-vertical');

    event.preventDefault();
  });
  
})()