'use strict';
var usermail = 'user@domain.com' ; /*實體會員的連絡信箱*/
var html_verify_email_ok_btn = '<div class="btn-popup"><a href="signup-physical-step2.html">確定</a></div>'; /*實體會員確認電子信箱的popup紐 - 點擊前往step2*/
var html_verify_email_edit_btn = '<div class="btn-popup"><a href="signup-physical-step2b.html">我要修改</a></div>'; /*實體會員確認電子信箱的popup紐 - 點擊前往step2b*/
var html_physical_sugnup_complete_btn = '<div class="btn-popup"><a href="/">確定</a></div></div>'; /*實體會員註冊完成鈕-點擊跳轉首頁*/

var timer = 300;
function countdown(){
  console.log("count");
  if ( timer > 0){
    timer -= 1;
    var mm = Math.floor(timer/60);
    var ss = timer%60;    
  }
  $("#mm").text(mm);
  $("#ss").text(ss);

  if(timer == 0){
    $("p").eq(0).text("簡訊驗證碼已超過時效，請重新驗證會員資料。"); 
    $("form").hide();
    $(".btn-full").not(".btn-timeout").hide();
    $(".btn-timeout").show();
  }
}

(function() {

  $(".signup-physical-step1").find(".btn-full").on('click', function(){
    var popupContent = '<h2 class="tcenter">電子郵件確認</h2><p class="tcenter">請確認「<span class="warning-color">' + usermail + '</span>」是否為你的電子郵件信箱</p><div class="tcenter">' + html_verify_email_ok_btn + html_verify_email_edit_btn + '</div>';
    HFeature.popup(popupContent);
    HFeature.popupOpen();
  });

  if ( $(".layout-content").hasClass("signup-physical-step2-2") ){
      setInterval( 'countdown();',1000);
  }

  $(".signup-physical-step3").find(".btn-full").on('click', function(){
    var popupContent = '<h2 class="tcenter">網路帳號開通成功</h2><p class="tcenter">設定密碼成功</p><p class="tcenter">您可以開始使用網路購物服務！</p><div class="tcenter">' + html_physical_sugnup_complete_btn;
    HFeature.popup(popupContent);
    HFeature.popupOpen();
  });

})()