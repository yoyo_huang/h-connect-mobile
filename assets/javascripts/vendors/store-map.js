var gmap_marker_icon = '../../assets/images/logo-black.svg';

function initialize() {
  var myLatlng = new google.maps.LatLng(storeLat,storeLng);

  var mapOptions = {
    zoom: 17,
    center: {lat: storeLat, lng: storeLng}
  };

  var map = new google.maps.Map(document.getElementById('store-map'),
      mapOptions);
  var image = {
    url: gmap_marker_icon,
    size: new google.maps.Size(46, 48),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(28, 48),
    scaledSize: new google.maps.Size(46, 48)
  };
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    icon: image
  });
  map.setOptions({ scrollwheel: false })
}

google.maps.event.addDomListener(window, 'load', initialize);