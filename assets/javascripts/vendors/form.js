'use strict';

(function() {
    $("body").on('click',".module-form-input", function(){
      $(this).parent().removeClass("error").find(".form-warning-text").remove();
    });

    $("body").on('click',".module-form-select", function(){
      $(this).removeClass("error").parent().find(".form-warning-text").remove();
    });

    $("body").on('click',".module-form-textarea", function(){
      $(this).parent().removeClass("error").find(".form-warning-text").remove();
    });
})()