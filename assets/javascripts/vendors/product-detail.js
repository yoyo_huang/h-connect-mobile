'use strict';
/*可從三處連到細節頁-列表、歷史記錄、最愛商品，三者的前後件商品會因此不同。從?origin判斷*/

var prevItemUrl = "product-detail.html";
var nextItemUrl = "product-detail.html";
var prevItemUrl_from_history = "product-detail.html?origin=history";
var nextItemUrl_from_history = "product-detail.html?origin=history";
var prevItemUrl_from_wishlist = "product-detail.html?origin=wishlist";
var nextItemUrl_from_wishlist = "product-detail.html?origin=wishlist";

/*for ajax 防呆*/
// var url_product_detail = '/views/product/product-detail.html';
// var url_payment = 'product-payment.html';

/*header back*/
var url_product_list = '/views/mypage/mypage-history.html';
var url_myhistory = '/views/mypage/mypage-history.html';
var url_wishlist = '/views/mypage/mypage-wishlist.html';


var popupContent_tutorial_detailpage = '操作教學...操作教學...操作教學...<br/><br/>左右滑動<br/>可看到更多的商品';/*會再修改*/

function searchToObject() {
  var pairs = window.location.search.substring(1).split("&"),
    obj = {},
    pair,
    i;

  for ( i in pairs ) {
    if ( pairs[i] === "" ) continue;

    pair = pairs[i].split("=");
    obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
  }

  return obj;
}

function switchGallery(){
  $(".layout-product-detail").toggleClass('focusing')
    .find(".btn-more").toggleClass('on');
    // console.log("switchGallery");
  if( $(".layout-product-detail").hasClass('focusing') ){
  }else{
    $('.layout-product-detail').animate({ scrollTop:( 0 )}, 500); 
  }
}

(function() {

  var data = {
        color: 0,
        size: 0
      },
      subData = {};


  //add to wishlist
  $(".layout-product-detail").on('click', '.btn-love', function() {
    $(this).toggleClass('liked');
  });

  //choose color
  $(".color-btn-wrap").on('click', '.btn-sq-color', function() {
    //sync 2 color-button-set
    var num = $(this).index();
    data.color = num;

    //change choosed color
    if( $(this).hasClass("choosed") ){
    }else{
      $(this).closest(".color-btn-wrap").find('.btn-sq-color').removeClass('choosed');
      $(this).addClass('choosed');      
    }
      //slide up buttton set
      $(".color-btn-wrap").toggleClass("close");
  });

  //choose size
  $(".size-btn-wrap").on('click', '.btn-sq-size', function() {
    //sync 2 size-button-set
    var num = $(this).index();
    data.size = num;

    //change choosed size
    if( $(this).hasClass("choosed") ){
    }else{
      $(this).closest(".size-btn-wrap").find('.btn-sq-size').removeClass('choosed');
      $(this).addClass('choosed');      
    }
      //slide up buttton set
      $(".size-btn-wrap").toggleClass("close");
  });

  //enter first time, show tutorial
  function showtutorial(){
    var isvisited = document.cookie.match("visited");
    if ( isvisited !== null ){
      // alert("you are visited");
    }else{
      // alert("isvisited == null, you didnot visited");
      HFeature.popup(popupContent_tutorial_detailpage);
      HFeature.popupOpen();
      $(".layout-popup-wrap").addClass("style-nocolor");
    }
  }
  showtutorial();
  //after read and close popup, i sure you are visited.
  $("body").on('click','.layout-popup-background,.layout-popup-wrap', function() {
    HFeature.popupClose();
    document.cookie = "visited";
  });

  //change header content
  $(".layout-header-bars").hide();
  $(".layout-header-goahead").hide();
  $(".layout-header-logo").addClass('style-numfont');

  //toggle info accordion,and scroll screen
  var headerHeight = hVariable.getHeaderHeight();
  var newoffset = parseInt( $(".layout-accordion").offset().top );
  $(".btn-info").on('click', function() {
    $(".layout-product-detail").toggleClass('info-open');
    //$(this).toggleClass('on');
    //if( $(this).hasClass('on') ){
      $('.layout-product-detail').animate({ scrollTop:( newoffset - headerHeight )}, 500); 
    //}
  });

  var scrollNow = 0;
  $(".layout-product-detail").on('scroll', function() {
    var scrollVal = $(this).scrollTop();
    // $(".btn-love,.btn-info").css("margin-top", scrollVal );
    if(scrollVal > scrollNow && scrollNow > 10){ /*scroll down*/
      if ( $(".layout-product-detail").hasClass("focusing") ){
      }else{
        switchGallery();
      }
    }
    if(scrollVal < scrollNow){ /*scroll up*/
      if ( scrollVal < 10 ){
        if ( $(".layout-product-detail").hasClass("focusing") ){
          switchGallery();
        }else{
        }
      }
    }
    // alert($(".layout-product-detail-imagewrap").height());
    if( (scrollVal + 180 + 52 + 45 ) > $(".layout-product-detail-imagewrap").height() ){
      // alert("oops");
      $(".btn-love,.btn-info").css("margin-top" , 0 - (scrollVal + 180 + 52 + 45 - parseInt($(".layout-product-detail-imagewrap").height())) );
    }else{
      $(".btn-love,.btn-info").css("margin-top" , 0 );
    }
  scrollNow = scrollVal;
  });



  //link to other pages

  $("body").swipe( {
    swipeLeft:function(event, direction, distance, duration, fingerCount) {
        alert("go to Next item");
        window.location.href = prevItemUrl;
    },
    swipeRight:function(event, direction, distance, duration, fingerCount) {
        alert("go to Prev item");
        window.location.href = nextItemUrl;
    }
  });
  $('.layout-header-goback').off('click');

  var obj=searchToObject();
  console.log(obj);
  
  //check if it is from wishlist or history
  if( obj.origin == "wishlist" ) {
    $(".layout-header-subtitle").hide();
    $(".layout-header-title").text("我的收藏");
    //define prev/next page & heaber back/forward btn
    prevItemUrl = prevItemUrl_from_wishlist;
    nextItemUrl = nextItemUrl_from_wishlist;
    $('.layout-header-goback').on('click',function() {
      window.location.href = url_wishlist;
    });
  }else
  if( obj.origin == "history" ) {
    $(".layout-header-subtitle").hide();
    $(".layout-header-title").text("瀏覽紀錄");
    //define prev/next page & heaber back/forward btn
    prevItemUrl = prevItemUrl_from_history;
    nextItemUrl = nextItemUrl_from_history;
    $('.layout-header-goback').on('click',function() {
      window.location.href = url_myhistory;
    });
  }else{
    $('.layout-header-goback').on('click',function() {
      window.location.href = url_product_list;
    });
  }

  //SEND ORDER DATA AND CHECK -
  //if you click twice, only if you have a different size/color order, or you cannot buy it twice.
  // $('.layout-product-detail-bottom').on('click', '.btn-buynow, .btn-addcart', function() {
  //   if($.isEmptyObject(subData) || data.color !== subData.color || data.size !== subData.size) {
  //     data.active = $(this).hasClass('btn-buynow') ? 'buy' : 'addtocart';
  //     $.ajax({
  //       url: url_product_detail,
  //       type: 'POST',
  //       data: data,
  //     })
  //     .done(function(success) {
  //       subData = {};
  //       $.extend(subData, data);
  //       // alert( data.active );
  //         if( data.active == 'buy' ){
  //           alert("go to payment page");
  //           window.location.href = url_payment;
  //         }else{            
  //           alert("cart num ++");
  //         }
  //     });
  //   }
  // });

})()